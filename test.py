import beaker.middleware
import bottle
import requests

authorized_emails = ("jordan.sherer@gmail.com",)

html = """
<!doctype html>
<html>
    <body>
        {{!body}}

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
        <script src="https://login.persona.org/include.js"></script>
        <script>
            function failedLogin(){
                alert("Login Failed");
            }

            function verifyAssertion(assertion) {
                $.post("/auth/verify", {assertion:assertion}).success(function(data){
                    if(!data.email || data.email === ""){
                        return failedLogin();
                    }

                    window.location.href = "/";
                }).error(failedLogin);
            }

            $(".login").click(function(e){
                if(e){ e.preventDefault(); }

                navigator.id.get(verifyAssertion);
            });
        </script>
    </body>
</html>
"""

@bottle.get("/")
def index():
    s = bottle.request.environ.get("beaker.session")
    logged_in = s.get("email", None)
    
    partial = """
    % if logged_in:
        <p>{{logged_in}} (<a href="/auth/logout">logout</a>)</p>
    % else:
        <p><a href="#" class="login">login</a></p>
    % end
    """
    body = bottle.template(partial, locals())
    return bottle.template(html, body=body) 


@bottle.get("/auth/logout")
def auth_logout():
    s = bottle.request.environ.get("beaker.session")
    s["email"] = None
    s.save()
    bottle.redirect("/")


@bottle.post("/auth/verify")
def auth_verify():
    s = bottle.request.environ.get("beaker.session")
    assertion = bottle.request.forms.get("assertion", "")
    data = {"assertion":assertion, "audience":"http://localhost:8080"}
    r = requests.post("https://verifier.login.persona.org/verify", data=data, verify=True)
    try:
        if r.status_code != 200:
            raise Exception("Invalid Assertion")

        json = r.json()
        if not json or "status" not in json or json["status"] != "okay":
            raise Exception("Invalid Assertion")

        email = json["email"] if "email" in json else ""
        if email not in authorized_emails:
            raise Exception("Unauthorized Email")

        s["email"] = email
        s.save()

        return {"success":True, "email":email}
    except Exception as e:
        return {"success":False, "message":str(e)}


def create_session_middleware(app):
    session_opts = {
        "session.type": "file",
        "session.cookie_expires": 300,
        "session.data_dir": "./data",
        "session.auto": True
    }

    return beaker.middleware.SessionMiddleware(app, session_opts)


if __name__ == "__main__":
    app = create_session_middleware(bottle.app())
    bottle.run(app=app, host="", port=8080, debug=True, reloader=True)
